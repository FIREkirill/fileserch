﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Security.AccessControl;

namespace FileSearch
{
    enum States { Start, Run, Pause, kill };

    public partial class Form1 : Form
    {
        volatile List<string> FilesToView;
        volatile string CurrentFile;
        volatile int sec;

        System.Timers.Timer timer1 = new System.Timers.Timer();
        readonly ManualResetEvent _busy = new ManualResetEvent(false);

        States State = States.Start;

        public Form1()
        {
            FilesToView = new List<string>();

            InitializeComponent();
            try
            {
                tbFolder.Text = Properties.Settings.Default.Folder;
                tbFileSpec.Text = Properties.Settings.Default.FileSpec;
                tbSearchText.Text = Properties.Settings.Default.FileText;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            timer1.Elapsed += OnTimedEvent;
            timer1.Interval = 1000;
        }

        private static void PopulateTreeView(TreeView treeView, IEnumerable<string> paths, char pathSeparator)
        {
            TreeNode lastNode = null;
            string subPathAgg;
            foreach (string path in paths)
            {
                subPathAgg = string.Empty;
                foreach (string subPath in path.Split(pathSeparator))
                {
                    subPathAgg += subPath + pathSeparator;
                    TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);
                    if (nodes.Length == 0)
                        if (lastNode == null)
                            lastNode = treeView.Nodes.Add(subPathAgg, subPath);
                        else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);
                    else
                        lastNode = nodes[0];
                }
            }
        }

        private string[] DirSearch(string sDir, string sFile)
        {
            List<string> res = new List<string>();
            try
            {
                try
                {
                    foreach (string f in Directory.GetFiles(sDir, sFile))
                    {
                        res.Add(f);
                    }
                }
                catch { }

                foreach (string d in Directory.GetDirectories(sDir))
                {
                    try
                    {
                        res.AddRange(DirSearch(d, sFile));
                    }
                    catch { }

                }
            }
            catch { }

            return res.ToArray();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int Progress = 0;
            string SearchString = tbSearchText.Text;
            string Folder = tbFolder.Text;
            string FileSpec = tbFileSpec.Text;
            sec = 0;

            this.timer1.Start();
            this.timer1.Enabled = true;

            try
            {
                if (!Directory.Exists(Folder))
                {
                    throw new Exception("Directory not found");
                }

                DirectoryInfo RootDir = new DirectoryInfo(Folder);
                
                var FileList = DirSearch(Folder, FileSpec);
                
                foreach (var file in FileList)
                {
                    _busy.WaitOne();

                    this.CurrentFile = file;
                    bool isExist = false;

                    if (File.Exists(file))
                    {
                        try
                        {
                            var CurrentFile = File.ReadAllLines(file);

                            foreach (var line in CurrentFile)
                            {
                                if (Regex.IsMatch(line, SearchString))
                                {
                                    isExist = true;
                                    break;
                                }
                            }
                        }
                        catch { }
                    }

                    this.Invoke(new MethodInvoker(delegate () { this.label1.Text = BuildLabelText(sec, file, true); }));

                    if (isExist)
                    {
                        FilesToView.Clear();
                        FilesToView.Add(file.Replace(Folder, RootDir.Name));

                        this.Invoke(new MethodInvoker(delegate () { DrawTree(); }));
                    }

                    bwSearchFiles.ReportProgress(Progress++);

                    if (bwSearchFiles.CancellationPending)
                    {
                        this.timer1.Stop();
                        this.timer1.Enabled = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DrawTree()
        {
            List<string> FilesToView = new List<string>(this.FilesToView);

            treeView1.BeginUpdate();

            PopulateTreeView(treeView1, FilesToView, '\\');

            treeView1.ExpandAll();
            treeView1.EndUpdate();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            switch (State)
            {
                case States.Start:
                    btnStart.Text = "Pause";
                    treeView1.Nodes.Clear();
                    FilesToView.Clear();
                    timer1.Enabled = true;
                    btnCancel.Enabled = true;
                    tbFolder.Enabled = false;
                    btnSelectFolder.Enabled = false;
                    tbFileSpec.Enabled = false;
                    tbSearchText.Enabled = false;

                    State = States.Run;
                    StartWorker();

                    break;

                case States.Run:
                    btnStart.Text = "Resume";
                    timer1.Enabled = false;

                    State = States.Pause;
                    PauseWorker();

                    break;

                case States.Pause:
                    btnStart.Text = "Pause";
                    timer1.Enabled = true;

                    State = States.Run;
                    StartWorker();

                    break;
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CancelWorker();
            State = States.kill;
            btnCancel.Enabled = false;
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            fbdFolder.SelectedPath = tbFolder.Text;
            fbdFolder.ShowDialog();
            tbFolder.Text = fbdFolder.SelectedPath;
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            sec++;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Save settings
            Properties.Settings.Default.Folder = tbFolder.Text;
            Properties.Settings.Default.FileSpec = tbFileSpec.Text;
            Properties.Settings.Default.FileText = tbSearchText.Text;
            Properties.Settings.Default.Save();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;

            btnCancel.Enabled = false;
            tbFolder.Enabled = true;
            btnSelectFolder.Enabled = true;
            tbFileSpec.Enabled = true;
            tbSearchText.Enabled = true;

            this.label1.Text = BuildLabelText(sec, (State != States.kill ? " Done!" : " Stoped!"));

            State = States.Start;
            btnStart.Text = "Start";
        }

        void StartWorker()
        {
            if (!bwSearchFiles.IsBusy)
            {
                bwSearchFiles.RunWorkerAsync();
            }
            _busy.Set(); // Unblock the worker 
            label1.Text = "In Progress...";
        }

        void PauseWorker()
        {
            _busy.Reset();  // Block the worker
            label1.Text = "Paused...";
        }

        void CancelWorker()
        {
            if (bwSearchFiles.IsBusy)
            {
                bwSearchFiles.CancelAsync();
                btnStart.Text = "Start";

                _busy.Set(); // Unblock worker so it can see that
            }
        }

        private string BuildLabelText(int Time, string Text, bool IsFile = false)
        {
            StringBuilder SBuilder = new StringBuilder();

            SBuilder.Append("time ellapsed: ");
            SBuilder.Append((String.Format("{0}:{1:00}:{2:00}", sec / 3600, sec / 60 % 60, sec % 60)).ToString());
            if (IsFile)
                SBuilder.Append(" current file: ");
            SBuilder.Append(Text);

            return SBuilder.ToString();
        }
    }

}
